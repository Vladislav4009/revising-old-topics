package by.shag.gritskevich.working.task3;

import by.shag.gritskevich.working.task2.Animal;
import by.shag.gritskevich.working.task2.Cat;
import by.shag.gritskevich.working.task2.Dog;

public class Task3 {

    public void checkClass(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        if (o.getClass().equals(Cat.class)
                || o.getClass().equals(Dog.class)
                || o.getClass().equals(Animal.class)) {
            throw new IllegalArgumentException();
        } else {
            System.out.println("Все ок");
        }
    }
}
