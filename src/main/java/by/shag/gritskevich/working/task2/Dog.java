package by.shag.gritskevich.working.task2;

public class Dog extends Animal {

    @Override
    public void getVoice() {
        System.out.println("Gav");
    }

    public void getVoice(String voice) {
        System.out.println(voice);
    }
}
