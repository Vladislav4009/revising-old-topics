package by.shag.gritskevich.working.task1;

public class Task1 {

    public void getStringMass(String[] mass) {
        int count = 0;
        while (mass.length > count) {
            System.out.print(mass[count] + " ");
            count++;
        }

        System.out.println(" ");

        for (int i = mass.length; i > 0; i--) {
            System.out.print(mass[i - 1] + " ");
        }

        System.out.println(" ");

        for (String word: mass ) {
            System.out.print(word.charAt(0) + " ");
        }

        System.out.println(" ");

        for (String word: mass) {
            System.out.print(word.charAt(word.length() - 1) + " ");
        }

        System.out.println(" ");

    }
}
