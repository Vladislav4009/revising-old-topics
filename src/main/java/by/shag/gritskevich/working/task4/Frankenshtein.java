package by.shag.gritskevich.working.task4;

public class Frankenshtein {

    private Head head;
    private Body body;
    private Arm leftArm;
    private Arm rightArm;
    private Leg leftLeg;
    private Leg rightLeg;

    public Frankenshtein(Head head, Body body, Arm leftArm, Arm rightArm, Leg leftLeg, Leg rightLeg) {
        this.head = head;
        this.body = body;
        this.leftArm = leftArm;
        this.rightArm = rightArm;
        this.leftLeg = leftLeg;
        this.rightLeg = rightLeg;
    }
}
