package by.shag.gritskevich.working.task4;

public enum Body {

    HUMAN_BODY,
    WOLF_BODY,
    CAT_BODY
}
