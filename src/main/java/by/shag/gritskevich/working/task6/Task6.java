package by.shag.gritskevich.working.task6;

import java.util.HashMap;
import java.util.Map;

public class Task6 {

    public Map<String, Integer> getMap(String string) {
        String word[] = string.split(" ");

        HashMap<String, Integer> hashMap = new HashMap<>();

        for (int i = 0; i < word.length; i++) {
            if (hashMap.containsKey(word[i])) {
                int count = hashMap.get(word[i]);
                hashMap.put(word[i], count + 1);
            } else {
                hashMap.put(word[i], 1);
            }
        }
        return hashMap;
    }
}
