package by.shag.gritskevich;

import by.shag.gritskevich.working.task1.Task1;
import by.shag.gritskevich.working.task2.Cat;
import by.shag.gritskevich.working.task2.Dog;
import by.shag.gritskevich.working.task3.Task3;
import by.shag.gritskevich.working.task4.*;
import by.shag.gritskevich.working.task5.Task5;
import by.shag.gritskevich.working.task6.Task6;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {

        Task1 task1 = new Task1();
        String[] mass = {"One", "Two", "Three", "Four"};
        task1.getStringMass(mass);

        Cat cat = new Cat();
        cat.getVoice();
        Dog dog = new Dog();
        dog.getVoice();
        String voice = "Hello";
        dog.getVoice(voice);

        Task3 task3 = new Task3();
        task3.checkClass(voice);

        Frankenshtein frankenshtein1 = new Frankenshtein(
                Head.HUMAN, Body.HUMAN_BODY, Arm.LEFT, Arm.LEFT, Leg.RIGHT, Leg.RIGHT);
        Frankenshtein frankenshtein2 = new Frankenshtein(
                Head.WOLF, Body.WOLF_BODY, Arm.LEFT, Arm.RIGHT, Leg.LEFT, Leg.RIGHT);

        Task5 task5 = new Task5();
        task5.getListWord(Arrays.asList("one", "two", "thee", "four", "five", "six", "seven", "eigth", "nine", "ten"));

        System.out.println(" ");

        String string =
                "в этой map ключом является одно слово значением число которое означает сколько раз этого одно слово встречается в предложении";
        Task6 task6 = new Task6();
        System.out.println(task6.getMap(string));
    }
}